# create google cloud vm template instance with gpu
gcloud beta compute instance-templates create gpu-template \
    --machine-type n1-standard-2 \
    --boot-disk-size 250GB \
    --accelerator type=nvidia-tesla-v100,count=1 \
    --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud \
    --maintenance-policy TERMINATE --restart-on-failure \
    --metadata startup-script='#!/bin/bash
    echo "Checking for CUDA and installing."
    # Check for CUDA and try to install.
    if ! dpkg-query -W cuda-9-0; then
      curl -O http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
      dpkg -i ./cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
      apt-get update
      apt-get install cuda-9-0 -y
    fi'

# create machine groups
gcloud compute instance-groups managed create ubuntu-gpu-group-zuli \
    --base-instance-name ubuntu-gpu-zuli \
    --size 2 \
    --template gpu-template \
    --zone europe-west4-a	

# GPU installation
# https://cloud.google.com/compute/docs/gpus/add-gpus