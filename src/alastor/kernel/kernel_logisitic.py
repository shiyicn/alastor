from basics import sigmoid
import numpy as np
from numpy import linalg as LA

class KernelLogistic(object):
    
    def __int__(self, method = 'newton'):
        self.method = 'newton'
    
    def loss(self, K, a, y, pen):
        sigma = sigmoid(K.dot(a) * y)
        n = K.shape[0]
        r = -1. / n * np.sum(np.log(sigma)) + pen / 2. * a.dot(K).dot(a)
        return r

    def gd(self, K, a, y, pen):
        sigma = sigmoid(K.dot(a) * y)
        n = K.shape[0]
        gradient = -1. / n * K.T.dot(y * (1 - sigma)) + pen * K.dot(a)
        return gradient

    def hessian(self, K, a, y, pen):
        sigma = sigmoid(K.dot(a) * y)
        n = K.shape[0]
        H = 1. / n * K.dot(sigma * (1-sigma) * K) + pen * K
        return H
    
    def newton(self, K, y, pen, a0 = None, max_iter = 10, epsilon = 1e-4):
        """
        
        Implement Newton's Method for Kernel Logistic Regression
        
        """

        n = K.shape[0]
        if a0 is None:
            a = np.zeros(n)
        else:
            a = a0.astype(float)

        for _ in range(max_iter):
            sigma = sigmoid(K.dot(a) * y)
            grad = -1. / n * K.T.dot(y * (1 - sigma)) + pen * K.dot(a)
            H = 1. / n * K.dot(sigma * (1 - sigma) * K) + pen * K
            # check the invertibility of hessian matrix H
            if 1. / np.linalg.cond(H) < 1e-10:
                H = H + epsilon * np.eye(n) # add perturbation
            # newton's method iteration
            a = a - np.linalg.inv(H).dot(grad)
        return a
    
    def sag(self, K, y, pen, coef_init = None, max_iter = 200, step = None, 
            using_line_search = False):
        """
        Implement the SAG algorithm. 
        To solve the optimization problem with the following form: 
                       n
        minimize 1/n * sum l_i(<K[i, :], x>, y_i) + g(x)
                       i=1
        where g(x) is a penalisation term, g(x) = pen / 2 * x.T * K * x
        
        Using the term : f_i = g + l_i(x)

                                     n
        x^{k+1} = x^{k} - step / n * sum y[i]^k
                                     i=1
        
        Note that 'sag' and 'saga' fast convergence is only guaranteed on 
        features with approximately the same scale
        """
        gradients = np.zeros(K.shape) # initializing the gradient
        n = K.shape[0] # 
        # upper bound of the Lipschitz constant for functions f_i
        L = pen * LA.norm(K) + 0.25 * np.max(np.diagonal(K.dot(K)))

        # From the source code scikit-learn
        # SAG theoretical step size is 1/16L but it is recommended to use 1 / L
        # see http://www.birs.ca//workshops//2014/14w5003/files/schmidt.pdf,
        # slide 65
        if step is None:
            step = 1. / L # the automatic setting of learning rate
        
        if coef_init is None:
            coef_init = np.zeros(n) # by default initialize coef_init to be zero
        
        if using_line_search :
            print('Line Search is not supported!')

        x = coef_init
        # define the termination criteria
        for _ in range(max_iter):
            k = (int) (np.random.uniform() * n) # sample an index
            gd_k = pen * K.dot(x) - (1 - sigmoid(K[k, :].dot(x))) * y[k] * K[k, :]
            gradients[k, :] = gd_k
            gradStochastic = np.mean(gradients, axis=1)
            x = x - step * gradStochastic
        return x

    def saga(self, K, y, pen, a0 = None, max_iter = 200):

        return None

    def fit(self, K, y, pen, a0 = None, max_iter = 10, epsilon = 1e-4, 
            method = 'newton'):
        """
        Function: 
        ---------
        Solve the following problem :
        \argmin_{a} 1/n \sum_{i=1} \log(1 + \exp\{-y_i * f(x_i)\}) + pen / 2 * a.T * K * a
        
        Fitted function f :
        f(x) = \sum_{i=1}^n K(x, x_i) * a_i
        probability(x) = \sigmoid(f(x))

        Args:
        -----
        K : the kernel matrix
        y : the label encoded in {-1, 1}
        pen : float, penalization coefficient
              pen >= 0
        max_iter : int, the maximum number of iterations to be executed
                   by default, max_iter = 10
        epsilon : float, perturbation level, by default epsilon = 1e-4
        a0 : array-like, float, a0 should be sufficiently close to the root
        method : string, 'newton' for Newton's Method, 'saga' for SAGA

        Return:
        -------
        a : matrix-like, logistic regression coefficient
        
        """

        if pen < 0:
            print("The penalization coefficient pen = {} is negative!\n".format(pen))

        # convert the kernel to double type
        K = K.astype(float)
        y = y.astype(float)

        n = K.shape[0]
        if not np.all((y == -1) + (y == 1)):
            print('The labels are not in {0, 1}.')

        # initialize a0
        if a0 is None:
            a = np.zeros(n)
        else:
            a = a0.astype(float)
        if method == 'newton':
            a = self.newton(K, y, pen, a0 = a0, max_iter = max_iter, epsilon = epsilon)
        elif method == 'sag':
            a = self.sag(K, y, pen, coef_init = a0, max_iter = max_iter)
        elif method == 'saga':
            print('SAGA is not supported yet.')
            a = None
        else: 
            print('Unsupported method : {} .\n'.format(method))
            return None
        
        return a
