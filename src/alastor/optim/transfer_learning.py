"""

Installation of PyTorch:
------------------------
# Install PyTorch (http://pytorch.org/)
from os import path
from wheel.pep425tags import get_abbr_impl, get_impl_ver, get_abi_tag
platform = '{}{}-{}'.format(get_abbr_impl(), get_impl_ver(), get_abi_tag())

accelerator = 'cu80' if path.exists('/opt/bin/nvidia-smi') else 'cpu'

import torch
print(torch.__version__)
print(torch.cuda.is_available())
------------------------

"""

import numpy as np
import matplotlib.pyplot as plt

import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import numpy as np
import torchvision

from torchvision import datasets, models, transforms
import time
import os
import copy
from tqdm import tqdm
from PIL import Image
import pickle

pretrained_name = 'polynet'

if pretrained_name is 'resnet':
  image_resize_size = 256
  image_crop_size = 224
elif pretrained_name is 'inception':
  image_resize_size = 328
  image_crop_size = 299
elif pretrained_name is 'polynet':
  image_resize_size = 356
  image_crop_size = 331
else:
  print('Invalid pretrained_name')

data_transforms = transforms.Compose([
    transforms.RandomRotation(90),
    transforms.Resize(image_resize_size),
    transforms.CenterCrop(image_crop_size),
    transforms.RandomHorizontalFlip(),
    transforms.RandomVerticalFlip(),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
])

data_transforms_val = transforms.Compose([
    #transforms.Resize((64, 64)),
    transforms.Resize(image_resize_size),
    transforms.CenterCrop(image_crop_size),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
])

# Training settings
args = easydict.EasyDict({
    'data' : 'bird_dataset',
    'batch_size' : 32,
    'epochs' : 100,
    'lr' : 0.1,
    'momentum' : 0.5,
    'seed' : 1,
    'log_interval' : 10,
    'experiment' : 'experiment'
})

use_cuda = torch.cuda.is_available()

# Create experiment folder
if not os.path.isdir(args.experiment):
    os.makedirs(args.experiment)

# Data initialization and loading
#from data import data_transforms

train_loader = torch.utils.data.DataLoader(
    datasets.ImageFolder(args.data + '/train_images',
                         transform=data_transforms),
    batch_size=args.batch_size, shuffle=True, num_workers=1)
val_loader = torch.utils.data.DataLoader(
    datasets.ImageFolder(args.data + '/val_images',
                         transform=data_transforms_val),
    batch_size=args.batch_size, shuffle=False, num_workers=1)

# Neural network and optimizer
# We define neural net in model.py so that it can be reused by the evaluate.py script
# from model import Net

image_datasets = {x: datasets.ImageFolder(os.path.join(args.data, x + '_images'),
                                          data_transforms)
                  for x in ['train', 'val']}

dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}

nclasses = 20

# Build dataloaders for training and validation process
dataloaders = {}
dataloaders['train'] = train_loader
dataloaders['val'] = val_loader


def transfer_learning(model, criterion, optimizer, scheduler, num_epochs=25):
    """
    
    Core function for transfer learning
    
    Params:
    -------
    model : pytorch model
            a pretrained pytorch neural network
    criterion : pytorch loss function
            a loss function like cross-entropy or norm loss
    optimizer : pytorch optimizer
            a pytorch optimizer like ADAM or SGD
    scheduler : pytorch learning rate scheduler
            a pytorch learning rate anealing scheduler 
    num_epochs : integer number
            total number 
    
    """
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model)

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model, best_model_wts

# Load pretrained model from pytorch
model_name = 'polynet'
model_conv = pretrainedmodels.__dict__[model_name](num_classes=1000, pretrained='imagenet')

for param in model_conv.parameters():
    param.requires_grad = False

# Change the last layer of the pretrainen network for classification to 
# out proper number of classes
num_ftrs = model_conv.last_linear.in_features
#num_ftrs = model_conv.classifier.in_features
model_conv.last_linear = nn.Linear(num_ftrs, nclasses)

# the final part of the pretrained network should be upgraded for gradient
for param in model_conv.stage_c.parameters():
  param.requires_grad = True

# Freeze all gradient of other convolutional layers
for param in model_conv.stage_c.parameters():
    param.requires_grad = True

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model_conv = model_conv.to(device)
criterion = nn.CrossEntropyLoss()
params_to_optimize = list(model_conv.last_linear.parameters()) + \
                      list(model_conv.stage_c.parameters())

#optimizer_conv = optim.SGD(params_to_optimize, lr=0.0001, momentum=0.9)
optimizer_conv = optim.Adam(params_to_optimize, lr=0.00001)
# Decay LR by a factor of 0.1 every 20 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=20, gamma=0.2)

model_conv_final, model_conv_best = transfer_learning(model_conv, criterion, optimizer_conv,
                         exp_lr_scheduler, num_epochs=25)

model_path = None

args['data'] = 'bird_dataset'
args['model'] = model_path
args['outfile'] = 'experiment/kaggle.csv'

model_conv.eval()
if use_cuda:
    print('Using GPU')
    model_conv.cuda()
else:
    print('Using CPU')

################################################################
## Evaluation part of fine-tuned model #########################
################################################################
test_dir = args.data + '/test_images/mistery_category'

def pil_loader(path):
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


output_file = open(args.outfile, "w")
output_file.write("Id,Category\n")
for f in tqdm(os.listdir(test_dir)):
    if 'jpg' in f:
        data = data_transforms_val(pil_loader(test_dir + '/' + f))
        data = data.view(1, data.size(0), data.size(1), data.size(2))
        if use_cuda:
            data = data.cuda()
        output = model_conv_best(data)
        pred = output.data.max(1, keepdim=True)[1]
        output_file.write("%s,%d\n" % (f[:-4], pred))

output_file.close()

print("Succesfully wrote " + args.outfile + ', you can upload this file to the kaggle competition website')