import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# The path contains the lib : ml_lib
if os.name == 'nt':
    root_path = 'C:\\Users\\Huang Zuli\\proj\\'
else:
    root_path = '~/proj/'

lib_path = os.path.join(root_path, 'ml_lib')
os.chdir(lib_path)

N = 100

r = np.sqrt(np.random.uniform(0, 1, N)) # radius
t = 2 * np.pi * np.random.uniform(0, 1, N) # angles
data1 = np.array([r * np.cos(t), r * np.sin(t)]) # points
data1 = data1.T

r2 = np.sqrt(3 * np.random.uniform(0, 1, N) + 1) # Radius
t2 = 2 * np.pi * np.random.uniform(0, 1, N) # Angle
data2 = np.array([r2 * np.cos(t2), r2 * np.sin(t2)]) # points
data2 = data2.T

X_train = np.concatenate((data1, data2), axis=0)
y_train = np.concatenate((np.ones(100), -np.ones(100)))

sigma = .4
pen = 0.1
K_train = kernel.kernels.gaussian_kernel(X_train, sigma)

ker_log = kernel.kernel_logisitic.KernelLogistic()
a = ker_log.fit(K_train, y_train, pen)

# visualization part
x_range = np.arange(-2, 2, 0.01)
y_range = np.arange(-2, 2, 0.01)

X_mesh, Y_mesh = np.meshgrid(x_range, y_range)
x_mesh_flattened = np.array([X_mesh.flatten(), Y_mesh.flatten()])
x_mesh_flattened = x_mesh_flattened.T

K_mesh = kernel.kernels.gaussian_kernel(X_train, sigma, X_new=x_mesh_flattened)

z_mesh_flattened = K_mesh.dot(a)
Z_mesh = z_mesh_flattened.reshape(X_mesh.shape)
plt.contour(X_mesh, Y_mesh, Z_mesh, [0])
plt.scatter(*X_train.T, c=y_train)
plt.show()
