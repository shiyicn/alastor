# avoid generate the pycache file
import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from time import time
import os
import scipy as sp
import pandas as pd

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/PhaseHarmonic1D_CC/')

if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.signal.convert_date_time import *
from ml_lib.signal.sum_dct import *

import ml_lib.signal.moving_filter as mf
import ml_lib.signal.filter_bank_generator as fb

from ml_lib.optim.metrics import *
from ml_lib.optim.linear_model import rolling_regression, plot_estimation

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
# -----------------------------------------------------------------------------

sys.dont_write_bytecode = True

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

# ----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'morlet'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
    tmp_dir_debug = 'C:\\Users\\Huang Zuli\\proj\\tmp\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
    tmp_dir_debug = '/Users/Huangzuli/proj/tmp/'
# ----------------------------------------------------------------------------

# ----------------- Currency Autoregressive with directly the price ---------------------------------- 
# set up internal parameters
# LEARNING_WINDOW = 250
LOWER_BOUND_DATE = create_date(2000, 1, 1) # start from 01/01/2000

market_types = ['G10', 'EM']
currencies_market = [os.listdir(os.path.join(root_path, market_type)) for market_type in market_types]

# set the path to save the figures
path2save_fig = os.path.join(datapath2save_root, 'regression', 
                             'normal',
                             'filtered_signal_{}.pdf'.format(LEARNING_WINDOW))
pdf2save = matplotlib.backends.backend_pdf.PdfPages(path2save_fig)
print('Save all figures into the pdf file located in : {}'.format(path2save_fig))

# set the path to save the numerical result
path2save_csv = os.path.join(datapath2save_root, 'regression', 
                             'normal',
                             'metrics_result_separate_{}.csv'.format(LEARNING_WINDOW))
out_csv_file = open(path2save_csv, "w")
print('Save the numerical results in the csv file located in : {}'.format(path2save_csv))
head_csv_title = ','.join( ['Type, Avg MA HR, Avg MA L2',
                            ','.join([str(x)+' HR' for x in np.arange(8)+1]), 
                            ','.join([str(x)+' L2' for x in np.arange(8)+1])])
out_csv_file.write(head_csv_title + '\n')

for iter_market, market_type in enumerate(market_types):
    hit_rate_market = 0
    l2_error_market = 0
    
    baseline_market = 0
    
    currencies_counting = 0
    
    for currency in currencies_market[iter_market]:
        
        metrics_log = ''
        currency = currency.replace('.csv', '')

        print('Start to test the currency {} with learning window size {}.'.format(currency, LEARNING_WINDOW))
        if currency == 'USDMYR':
            print('Currency instance : {} is skipped.'.format(currency))
            continue
        
        currencies_counting += 1

        flagOfTitle = market_type + ' ' + currency

        file2load_path = os.path.join(root_path, market_type, currency + '.csv')
        print('Load file from path {}.'.format(file2load_path))
         
        data_loaded = pd.read_csv(file2load_path)
        # filter the time series by the starting time
        data_loaded = filter_by_time(data_loaded['Date'], data_loaded, lower_bound=LOWER_BOUND_DATE)
        
        # set the index of loaded time series to excel format date
        data_loaded.set_index('Date', inplace=True)
        X_init = np.log(data_loaded['PX_LAST'])
        X_init = (X_init.diff()) # convert to daily log-difference return 

        # delete NaN from the currency data
        if not np.all(np.isnan(X_init)):
            print('NaN value exists in X_init, in total {}.'.format(np.sum(np.isnan(X_init))))

        X_dates = X_init.index.values[np.logical_not(np.isnan(X_init))]
        X_dates = excel2pandas(X_dates)
        print("Compute From [{}] to [{}].".format(X_dates[0], X_dates[-1]))
        X_init = X_init[np.logical_not(np.isnan(X_init))]
        plot_scattering_signal(X_init, X_date=X_dates, flagOfTitle=flagOfTitle, path2save_fig='', pdf2save=pdf2save)
        T = len(X_init) # total length of the time series

        # calculate the next day return
        next_return = X_init.shift(periods=-1)

        # ----------------------------------------------------------------------------
        # Benchmark 1: 
        fltrs = fb.gen_filter_bank(T, 8, filter_type='box')
        # fltrs = [np.ones(16) / np.sqrt(16)]
         
        X_filtered = [mf.filtering(X_init, fltr, mark_nan = True) for fltr in fltrs]
        X_filtered = pd.DataFrame(X_filtered).T
         
        # save a tmp file for filtered signal
        X_filtered.to_csv(os.path.join(tmp_dir_debug, 'normal', 
                                       currency + '_filtered.csv'))
        X_filtered2plot = np.zeros((X_filtered.T.shape[0], X_filtered.T.shape[1], 2))
        X_filtered2plot[..., 0] = X_filtered.T
        plot_mra(X_filtered2plot, flagOfTitle=flagOfTitle, X_date=excel2pandas(X_filtered.index), pdf2save=pdf2save)
         
        r_hat_avg = X_filtered.mean(axis=1, skipna=False)
        
        baseline_market += np.array([hit_rate(r_hat_avg, next_return), 
                                     rmse(r_hat_avg, next_return)])
        metrics_log += str(hit_rate(r_hat_avg, next_return)) + ','
        metrics_log += str(rmse(r_hat_avg, next_return)) + ','
         
        r_hat_rolling, coefs_rolling = rolling_regression(X_filtered, 
                                                          next_return, 
                                                          flag_separate = True,
                                                          learning_window=LEARNING_WINDOW)
        
        coefs_rolling.to_csv(os.path.join(tmp_dir_debug, 
                                          'normal',
                                          currency + '_filtered_coefs_{}.csv'.format(LEARNING_WINDOW)))
        coefs_rolling2plot = np.zeros((coefs_rolling.T.shape[0], coefs_rolling.T.shape[1], 2))
        coefs_rolling2plot[..., 0] = coefs_rolling.T
        plot_mra(coefs_rolling2plot, flagOfTitle=flagOfTitle, X_date=excel2pandas(coefs_rolling.index), pdf2save=pdf2save)
        
        hit_rate_market += np.array(hit_rate(r_hat_rolling, next_return))
        l2_error_market += np.array(rmse(r_hat_rolling, next_return))
        
        metrics_log += ','.join(map(str, hit_rate(r_hat_rolling, next_return))) + ','
        metrics_log += ','.join(map(str, rmse(r_hat_rolling, next_return))) + ','
        
        plot_estimation(r_hat_rolling, dates=excel2pandas(r_hat_rolling.index.values), 
                        flag_of_title=flagOfTitle, pdf2save=pdf2save)

        metrics_log = flagOfTitle + ',' + metrics_log
        
        out_csv_file.write(metrics_log + '\n')
        plt.close()
    
    # start to write the average result
    hit_rate_market /= currencies_counting
    l2_error_market /= currencies_counting
    baseline_market /= currencies_counting
    metrics_log = ','.join([market_type, 
                           ','.join(map(str, baseline_market)),
                           ','.join(map(str, hit_rate_market)), 
                           ','.join(map(str, l2_error_market))])
    out_csv_file.write(metrics_log + '\n')
    

out_csv_file.close()
pdf2save.close()
