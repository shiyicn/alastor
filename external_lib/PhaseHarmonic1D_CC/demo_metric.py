# PhaseHarmonic1D
# Copyrigth (c) by Gaspar Rochette, Roberto Leonarduzzi and Stéphane Mallat

# PhaseHarmonic1D is licensed under a
# Creative Commons Attribution-NonCommercial 4.0 International License.

# You should have received a copy of the license along with this
# work. If not, see <https://creativecommons.org/licenses/by-nc/4.0/>.

import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time

from metric import PhaseHarmonicCoeff

do_cuda = False

#----- Analysis parameters -----
T = 2**10  # Data length
J = 9      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave


# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'battle_lemarie'

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'compute_second_order': compute_second_order}


# Create analysis object
phi = PhaseHarmonicCoeff(J, Q, T,  **phe_params)

#----- Create data -----

x = np.random.randn(T)

# Convert to torch tensor of size 1 x 1 x T x 2, where last dimension contains real
# and imaginary part
x_torch = torch.DoubleTensor(x[None, None, :])
x_torch = torch.stack((x_torch, torch.zeros_like(x_torch)), dim=-1)

#----- Analysis -----

# Move to cuda if required:
if do_cuda:
    x_torch = x_torch.cuda()
    phi = phi.cuda()

# Do the analysis
y_torch = phi(x_torch, output_sparse_p=True)


# y_torch is a 4-tuple (avg, cov, var, sparse) containing average, covariances, variances and sparse coefficients
# Each of the four outputs are 2-tuples (first, second) with coefficients of first and second order coefficients
# Each coefficient tensor is of size 1 x 1 x ncoeff x 2, where the last dimmension contains real and imaginary part.
# For average and sparse coefficients, ncoeff = J + 1
# For covariance coefficients, it depends on the selected coefficients in variable coeff_select. However, all coefficients
# are concatenated in the 3rd dimension.

# Information about the covariance coefficients is contained in the dictionary idx_info:
idx_info = phi.idx_info
# This is a 2-tuple with dictionaries for the first and second order.
# For each order, you can select the type of coefficients, and then the values of k and indices of the wavelets that where involved.
# For example:
xi_harmonic_first = idx_info[0]['harmonic']['xi_idx']  # Wavelet indices for each harmonic covariance coefficient
k_harmonic_first = idx_info[0]['harmonic']['k']   # K values for each harmonic covariance coefficient


# You can get the different covariance coefficients simply by doing:
cov = y_torch[1]
cov_first_dict = phi.coeffs_to_dict(cov)[0]
cov_first_harmonic = cov_first_dict['harmonic'] # Harmonic covariance coefficients
cov_first_harmod   = cov_first_dict['harmod']   # Modulus coefficients
cov_first_mixed    = cov_first_dict['mixed']    # Mixed coefficients
cov_first_jeqmod   = cov_first_dict['jeqmod']   # Variance coefficients (j=j', k=k'=0)

print(cov_first_harmonic.shape)
print(cov_first_dict['harmonic'].shape)
print(phi.idx_info[0]['harmonic']['k'].shape)
print(cov_first_dict['harmod'].shape)
print(phi.idx_info[0]['harmod']['k'].shape)
print(cov_first_dict['mixed'].shape)
print(phi.idx_info[0]['mixed']['k'].shape)
print(cov_first_dict['jeqmod'].shape)
print(phi.idx_info[0]['jeqmod']['k'].shape)
