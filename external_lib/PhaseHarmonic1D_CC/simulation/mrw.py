# Synthesis of multifractal random walk and derived processes.
#
# Roberto Fabio Leonarduzzi
# January, 2019

import numpy as np
from simulation.fbm import fbm
from simulation.pzutils import gaussian_cme
from numpy.fft import fft, ifft
import math
import matplotlib.pyplot as plt

def mrw(shape, H, lam, L,  sigma=1):
    '''
    Create a realization of fractional Brownian motion using circulant
    matrix embedding.

    Inputs:
      - shape: if scalar, it is the  number of samples. If tuple it is (N, R), the
               number of samples and realizations, respectively.
      - H (scalar): Hurst exponent.
      - lambda (scalar): intermittency parameter.
      - L (scalar): integral scale.
      - sigma (scalar): variance of process.

    Outputs:
      - mrw: synthesized mrw realizations. If 'shape' is scalar, fbm is of shape (N,).
             Otherwise, it is of shape (N, R).

    References:
      - Bacry, Delour, Muzy, "Multifractal Random Walk", Physical Review E, 2001
    '''

    try:
        N, R = shape
        do_squeeze = False
    except TypeError:  # shape is scalar
        N, R = shape, 1
        do_squeeze = True

    # Is 0.5 or 0 the lower bound ? Search biblio
    if not 0 <= H <= 1:
        raise ValueError('H must satisfy 0 <= H <= 1')

    if L > N:
        raise ValueError('Integral scale L is larger than data length N')

    ## 1) Gaussian process w
    w = gaussian_w(N, R, L, lam)

    # Adjust mean to ensure convergence of variance
    r = 1/2  # see Bacry, Delour & Muzy, Phys Rev E, 2001, page 4
    w = w - np.mean(w, axis=0) - r * lam**2 * np.log(L)

    ## 2) fGn e
    fgn= np.diff(fbm((N + 1, R), H, sigma), axis = 0)

    ## 3) mrw
    mrw = np.cumsum(fgn * np.exp(w), axis=0)

    return mrw.squeeze() if do_squeeze else mrw


def skewed_mrw(shape, H, lam, L,  K0=1, alpha=1, sigma=1, dt=1, beta=1, do_mirror=False):
    '''
    Create skewed mrw as in Pochart & Bouchaud
    Assumes \Delta t = 1, so no parameter beta is needed.
    '''

    try:
        N, R = shape
        do_squeeze = False
    except TypeError:  # shape is scalar
        N, R = shape, 1
        do_squeeze = True

    # Is 0.5 or 0 the lower bound ? Search biblio
    if not 0 <= H <= 1:
        raise ValueError('H must satisfy 0 <= H <= 1')

    if L / dt > N:
        raise ValueError('Integral scale L/dt is larger than data length N')

    ## 1) Gaussian process w
    w = gaussian_w(N, R, L, lam, dt)

    # Adjust mean to ensure convergence of variance
    r = 1  # see Bacry, Delour & Muzy, Phys Rev E, 2001, page 4
    w = w - np.mean(w, axis=0) - r * lam**2 * np.log(L / dt)

    ## 2) fGn e
    fgn = np.diff(fbm((2*N + 1, R), H, sigma, dt), axis = 0)
    #fgn = sigma * math.sqrt(dt) * np.random.randn(2*N, R)

    ## 3) Correlate components
    past = skewness_convolution(fgn, K0, alpha, beta, dt)
    wtilde = w - past

    ## 4) skewed mrw
    smrw = np.cumsum(fgn[N:] * np.exp(wtilde), axis=0)

    if do_squeeze:
        smrw = smrw.squeeze()

    if do_mirror:
        past_mirror = skewness_convolution(-fgn, K0, alpha, beta, dt)
        wtilde_mirror = w - past_mirror
        smrw_mirror = np.cumsum(-fgn[N:] * np.exp(wtilde_mirror), axis=0)
        if do_squeeze:
            smrw_mirror = smrw_mirror.squeeze()
        return smrw, smrw_mirror
    else:
        return smrw

def gaussian_w(N, R, L, lam, dt=1):
    '''
    Auxiliar function to create gaussian process w
    '''
    kmax = int(L / dt)
    k = np.arange(kmax)
    rho = np.ones((N))
    rho[:kmax] = L / (k + 1) / dt
    cov = (lam ** 2) * np.log(rho)
    w = gaussian_cme(cov, N, R)
    return w

def skewness_convolution(e, K0,  alpha, beta=1, dt=1):
    '''
    Noise e should be of length 2*N, with "N false past variables" at the beginning
    to avoid spurious correlations due to cutoffs in convolution.
    '''
    N, R = e.shape
    N = N // 2

    tau = np.arange(1, N+1)
    Kbar=np.zeros((2*N))
    Kbar[1:N+1] = K0 / (tau**alpha) / (dt**beta)
    skew_conv = np.real(ifft(fft(Kbar[:, None], axis=0) *
                             fft(e, axis=0), axis=0))
    return skew_conv[N:]

def skewness_convolution_dumb(e, K0, alpha, beta=1, dt=1):
    '''
    Direct and inefficient calculation for testing purposes.
    Receives "true" input noise of size N.
    '''
    N, R = e.shape

    def K(i,j):
        return K0 / (j-i)**alpha / dt**beta

    scorr = np.zeros((N, R))
    for k in range(N):
        for i in range(k):
            scorr[k, :] += K(i, k) * e[i, :]
    return scorr
